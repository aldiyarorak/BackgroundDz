﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;


namespace WpfApplication4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            comboBox.ItemsSource = typeof(Colors).GetProperties();
            string clr = Read();
            Color readColor = (Color)ColorConverter.ConvertFromString(clr);
            this.Background = new SolidColorBrush(readColor);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Color selectedColor = (Color)(comboBox.SelectedItem as PropertyInfo).GetValue(1, null);
            this.Background = new SolidColorBrush(selectedColor);
            Write(selectedColor);
        }
        private string Read()
        {
            string path = "TextFile1.txt";
            string line = "";
            using (StreamReader sr = new StreamReader(path))
            {
                line = sr.ReadToEnd();
            }
            return line;
        }
        private void Write(Color color)
        {
            string path = "TextFile1.txt";
            using (StreamWriter sw = new StreamWriter(path))
            {
                File.WriteAllText(path, String.Empty);
                sw.WriteLine(color.ToString());
            }
        }
    }
}
